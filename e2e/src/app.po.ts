import { browser, by, element, Key } from 'protractor';

export class AppPage {
  /*
      I put them as a comment.
      This is my purpose to test if it works.
  */
  // navigateTo() {
  //   // Navigate to the home page of the app
  //   return browser.get(browser.baseUrl);
  // }

  // getTitleText() {
  //   // Get the home page heading element reference
  //   return element(by.css('app-root router-outlet')).getText();
  // }

  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl);
  }

  async getTitleText(): Promise<string> {
    return element(by.css('app-root router-outlet')).getText();
  }

  // TODO fetch the home page
  async getHomePage(): Promise<string>{
    return element(by.css(''))
  }

  // TODO fetch the navbar page


  selectNextKey() {
    browser
      .actions()
      .sendKeys(Key.ARROW_RIGHT)
      .perform();
  }

  selectPrevKey() {
    browser
      .actions()
      .sendKeys(Key.ARROW_LEFT)
      .perform();
  }
}

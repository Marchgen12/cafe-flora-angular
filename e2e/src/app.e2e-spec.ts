import { CustomerHomePageComponent } from './../../src/app/customer-home-page/customer-home-page.component';
import { CustomerNavBarComponent } from './../../src/app/customer-nav-bar/customer-nav-bar.component';
import { AppPage } from './app.po';
import { browser, by, element, logging } from 'protractor';

describe('Open the link', () => {
  beforeEach(() => {
    browser.waitForAngularEnabled(false);
    browser.get(browser.baseUrl);
  });
})

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('testing Navbar', () => {
    browser.sleep(2000).then(function () {
      checkNavbarTexts();
      navigateToListPage();
    });
  });

  function checkNavbarTexts() {
    element(by.id('home')).getText().then(function (text) {
      expect(text).toEqual('Home');
    });
    element(by.id('menu')).getText().then(function (text) {
      expect(text).toEqual('Menu');
    });
    element(by.id('cart')).getText().then(function (text) {
      expect(text).toEqual('Cart');
    });
  }

  function navigateToListPage() {
    element(by.id('list-home')).click().then(function () {
      browser.sleep(2000).then(function () {
        browser.getCurrentUrl().then(function (actualUrl) {
          expect(actualUrl.indexOf('list') !== -1).toBeTruthy(); // check the current url is list
        });
      });
    });
  }

  it('should display welcome message', async () => {
    await page.navigateTo();
    expect(await page.getTitleText()).toEqual('');
  });



  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});

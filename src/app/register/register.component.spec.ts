import { HttpClient } from '@angular/common/http';
import { HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs';
import { LoginComponent } from '../login/login.component';
import { Customer } from '../services/customer';
import { CustomerService } from '../services/customer.service';

import { RegisterComponent } from './register.component';

describe('RegisterComponent', () => {

  // let MockUser = {
  //   email : "email@email.com",
  //   password : "password",
  //   firstName : "John",
  //   lastName : "Smith",
  //   address : "123 Main St",
  //   city : "New City",
  //   state : "NY",
  //   zipCode : "01102",
  //   phone : "616 314 2589",
  //   creditCardNumber : "1111111111111111",
  //   creditCardName : "John Smith",
  //   creditCardMonth : 1,
  //   creditCardYear : 2021,
  //   creditCardCode : 999
  // }


  class MockService {
    RegisterCustomer(){return mockClient.post()}
  }

  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let mockClient: {get: jasmine.Spy, post: jasmine.Spy, put: jasmine.Spy, delete: jasmine.Spy}
  let router: Router;
  let crServ: CustomerService;
  // let httpMock: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[
        RouterTestingModule.withRoutes([
          // {path: 'login', component: LoginComponent},
          {path: 'register', component: RegisterComponent}
        ]),
        // [HttpTestingController],
        [FormsModule]
      ],
      declarations: [ RegisterComponent, LoginComponent],
      providers: [
        {provide: CustomerService, useClass:MockService},
        {provide: HttpClient, useValue: mockClient}
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    crServ = TestBed.inject(CustomerService);
    mockClient = TestBed.get(HttpClient);
    router = TestBed.inject(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fill up form to create a user', ()=>{
    spyOn(crServ, 'RegisterCustomer').and.returnValue(Observable.create(observer=>{
      observer.next("string");
    }));  
    
    const navigateSpy = spyOn(router, 'navigate');
    component.customer.firstName = "John6";
    component.customer.lastName = "Smith6";
    component.customer.email = "email6@email.com";
    component.customer.password = "password";
    component.customer.address = "123 Main St";
    component.customer.city = "New City";
    component.customer.state = "NY";
    component.customer.zipCode = "01102";
    component.customer.phone = "616 314 2586";
    component.customer.creditCardNumber = "1111111111111116";
    component.customer.creditCardName = "John Smith";
    component.customer.creditCardMonth = 1;
    component.customer.creditCardYear = 2021;
    component.customer.creditCardCode = 999;

    fixture.detectChanges();

    component.registerUser();
    expect(navigateSpy).toHaveBeenCalledWith(['/login']);
  });
    

  it('should go back to login page', ()=>{
    const navigateSpy = spyOn(router, 'navigate');
    component.goLogin();
    expect(navigateSpy).toHaveBeenCalledWith(['/login']);
  });
});

import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgModel, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Customer } from '../services/customer';
import { CustomerService } from '../services/customer.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  customer = new Customer();

  constructor(private CustomerService: CustomerService,  private router:Router) { }

  ngOnInit(): void {
    localStorage.setItem("customerInfo", null);
  }

  goLogin(){
    this.router.navigate(['/login']);
  }

  registerUser(){
    this.CustomerService.RegisterCustomer(this.customer.email, this.customer.password, this.customer.firstName, this.customer.lastName, 
        this.customer.address, this.customer.city, this.customer.state, this.customer.zipCode, this.customer.phone, this.customer.creditCardName,
        this.customer.creditCardNumber, this.customer.creditCardMonth, this.customer.creditCardYear, this.customer.creditCardCode).subscribe(
      Response=>{
        alert("Account created!");
        console.log("Account created!");
        this.router.navigate(['/login']);
      },
      error=>{
        alert("Account creation failed");
        console.log("Account creation failed");
        this.router.navigate(['/register']);
      }
    )
  }

}
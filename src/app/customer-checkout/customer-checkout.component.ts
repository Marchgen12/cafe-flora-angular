import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, SystemJsNgModuleLoader } from '@angular/core';
import { Router } from '@angular/router';
import { CouponService } from '../services/coupon.service';
import { CustomerService } from '../services/customer.service';
import { OrderService } from '../services/order.service';
import { FormControl, FormGroup } from '@angular/forms';
import { OrderitemService } from '../services/orderitem.service';
import { OrderItem } from '../services/orderitem';
import { Customer } from '../services/customer';
import { Order } from '../services/order';
import { Coupon } from '../services/coupon';
import { Distribution } from '../services/distribution';
import { DistributionService } from '../services/distribution.service';

@Component({
  selector: 'app-customer-checkout',
  templateUrl: './customer-checkout.component.html',
  styleUrls: ['./customer-checkout.component.css']
})
export class CustomerCheckoutComponent implements OnInit {

  constructor(private router:Router, 
    private custServ:CustomerService, 
    private ordServ:OrderService,
    private ordItemServ:OrderitemService, 
    private distServ:DistributionService,
    private coupServ:CouponService) { }

  subtotal:number = 0;
  couponDiscount:number = 0;
  deliveryTip:number = 0;
  tax:number = (this.subtotal+this.couponDiscount)*.05;
  total:number = this.subtotal+this.couponDiscount+this.deliveryTip+this.tax;
  
  customer:Customer;
  order:Order;
  couponObject:Coupon = null;
  distrObject:Distribution;
  distId:number;
  tempcoup:Coupon;
  orderId:number;
  
  coupAmount:number = 0;
  couponitem:number = 0;
  foundItem:boolean = false;
  isVisible:boolean = true;
  
  @Input() coupon:string;
  @Output() couponChange = new EventEmitter<String>();

  @Input() deltip:number;
  @Output() tipChange = new EventEmitter<number>();

  @Input() distr:number;
  @Output() distrChange = new EventEmitter<number>();

  //on loading of screen we get the session data for customer and order, set the page variables from that data
  ngOnInit(): void {
    this.customer = JSON.parse(localStorage.getItem("customerInfo"));
    this.order = JSON.parse(localStorage.getItem("customerOrder"));
    if (this.customer == null) {
      this.router.navigate(['/login']);
    }

    this.orderId = this.order.orderId;
    this.subtotal = this.order.subtotal;
    this.tax = (this.subtotal+this.couponDiscount)*.05;
    this.total = this.subtotal+this.couponDiscount+this.deliveryTip+this.tax;

    this.distr = 1;
    this.distId = 1;
  }

  //if they update the coupon code field we try to get the coupon from the database
  newCoupon(UpdatedValue : string) :void 
  { 
    this.couponDiscount = 0;
    this.tax = (this.subtotal+this.couponDiscount)*.05;
    this.total = this.subtotal+this.couponDiscount+this.deliveryTip+this.tax;
    this.couponObject = null; 
    this.couponGetByCode(UpdatedValue); 
  } 

  //if they update the tip field we apply it to the page variable and update the order value
  newTip(UpdatedValue : number) :void 
  { 
    this.updateTip(UpdatedValue);
    if (UpdatedValue == null) {
      this.deliveryTip = 0;
      this.order.deliveryTip = 0.00;
    } else {
      this.order.deliveryTip = UpdatedValue;
    }
    
  } 

  //if they change distribution we change whether tip is visible and set the order value
  newDistr(UpdatedValue : number) :void
  {
    this.distId = UpdatedValue;
    if (UpdatedValue == 1) {
      this.deliveryTip = 0;
      this.order.deliveryTip = 0.00;
      this.total = this.subtotal+this.couponDiscount+this.deliveryTip+this.tax;
      this.isVisible = true;
    } else {
      this.deliveryTip = 0;
      this.deltip = null;
      this.isVisible = false;
      this.order.deliveryTip = 0.00;
      this.total = this.subtotal+this.couponDiscount+this.deliveryTip+this.tax;
    }
  }

  //this is where we get the coupon code and then see if it is for an item that they have in their cart 
  couponGetByCode(coupon:String) {
    this.coupServ.getCouponById(coupon).subscribe(
      response => {
        this.tempcoup = response;
        this.couponitem = response.menuItem.itemId;
        this.coupAmount = response.couponAmount;
        if (this.compareItems(this.couponitem, this.coupAmount)) {
          this.couponDiscount = this.coupAmount;
          this.couponObject = this.tempcoup;          
          this.tax = (this.subtotal+this.coupAmount)*.05;
          this.total = this.subtotal+this.coupAmount+this.deliveryTip+this.tax;
        }
      }
    )
  }

  //this is where we compare the items in the cart to the coupon's item to see if they qualify for the discount, then update the page accordingly
  compareItems(couponitem:number, coupAmount:number):boolean {
    this.ordItemServ.getOrderItemsByOrderId(this.orderId).subscribe(
      response => {
        let itemlist:OrderItem[] = response;
        for (var temp of itemlist) {
          if (temp.menuItem.itemId == couponitem) {
            this.couponDiscount = this.coupAmount;
            this.couponObject = this.tempcoup; 
            this.tax = (this.subtotal+this.coupAmount)*.05;
            this.total = this.subtotal+this.coupAmount+this.deliveryTip+this.tax;
            return true;
          } 
        }
      }
    )
    return false;
  }

  //this is where we apply the tip to the page/order variable
  updateTip(deltip:number) {
    this.deliveryTip = deltip;
    this.total = this.subtotal+this.couponDiscount+this.deliveryTip+this.tax;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.couponGetByCode(changes.coupon.currentValue);
  }

  //this is where we submit the order to the database, if successful we go to the payment page
  submitorder() {
    this.distServ.getDistributionById(this.distId).subscribe(
      response => {
        this.distrObject = response;
        this.order.coupon = this.couponObject;
        this.order.distribution = this.distrObject;
        this.order.deliveryTip = this.deliveryTip;
        this.order.tax = this.tax;
        this.order.total = this.total;
        localStorage.setItem("customerOrder", JSON.stringify(this.order));
        this.ordServ.updateOrder(this.order).subscribe (
          response => {
            this.router.navigate(['/payment']);
          }
        )
      }
    )
  }

  //redirect back to the cart
  backtocart() {
    this.router.navigate(['/cart']);
  }

}

import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs';
import { CartComponent } from '../cart/cart.component';
import { CustomerHomePageComponent } from '../customer-home-page/customer-home-page.component';
import { CustomerMenuComponent } from '../customer-menu/customer-menu.component';
import { CustomerNavBarComponent } from '../customer-nav-bar/customer-nav-bar.component';
import { CustomerOrderVerificationComponent } from '../customer-order-verification/customer-order-verification.component';
import { CustomerPaymentComponent } from '../customer-payment/customer-payment.component';
import { ForgetComponent } from '../forget/forget.component';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';
import { Coupon } from '../services/coupon';
import { CouponService } from '../services/coupon.service';
import { CouponStatus } from '../services/couponstatus';
import { Customer } from '../services/customer';
import { Distribution } from '../services/distribution';
import { DistributionService } from '../services/distribution.service';
import { MenuItem } from '../services/menuitem';
import { Order } from '../services/order';
import { OrderService } from '../services/order.service';
import { OrderItem } from '../services/orderitem';
import { OrderitemService } from '../services/orderitem.service';
import { OrderStatus } from '../services/orderstatus';
import { CustomerCheckoutComponent } from './customer-checkout.component';

describe('CustomerCheckoutComponent', () => {
  
  class MockService {
    getCouponById() {return mockClient.get()}
    getOrderItemsByOrderId() {return mockClient.get()}
    getDistributionById() {return mockClient.get()}
    updateOrder() {return mockClient.post()}
  }

  let component: CustomerCheckoutComponent;
  let fixture: ComponentFixture<CustomerCheckoutComponent>;
  let mockClient: {get: jasmine.Spy, post: jasmine.Spy, put: jasmine.Spy, delete: jasmine.Spy}
  let router:Router;
  let ordServ:OrderService;
  let ordItemServ:OrderitemService;
  let distServ:DistributionService;
  let coupServ:CouponService;
  let store = {};
  let httpMock: HttpTestingController;

  let dummyCustomerData2:Customer = {
    customerId : 1,
    email : "email@email.com",
    password : "password",
    firstName : "John",
    lastName : "Smith",
    address : "123 Main St",
    city : "New City",
    state : "NY",
    zipCode : "01102",
    phone : "616 314 2589",
    creditCardNumber : "1111111111111111",
    creditCardName : "John Smith",
    creditCardMonth : 1,
    creditCardYear : 2021,
    creditCardCode : 999
    }
  
    let dummyOrderStatusData2:OrderStatus = {
      statusId:1,
      statusName:"Building"
    }
    
    let dummyDistributionData2:Distribution = {
      distributionId : 1,
      distributionMethod : "Delivery"
    }
    
    let dummyCouponStatusData2:CouponStatus = {
      couponStatusId : 1,
      couponStatus : "Active"
    }
    
    let dummyMenuItemData2:MenuItem = {
      itemId : 1,
      itemName : "Bacon",
      itemDescription : "Nice Bacon",
      itemCost : 4.50,
      itemImageUrl : "thisurl"
    }
    
    let dummyCouponData2:Coupon = {
      couponCode : "baconspecial",
      menuItem : dummyMenuItemData2,
      couponStatus : dummyCouponStatusData2,
      couponAmount : -1.00
    }
    
    let dummyOrderData2:Order = {
      orderId : 1,
      customer : dummyCustomerData2,
      orderStatus : dummyOrderStatusData2,
      distribution : dummyDistributionData2,
      coupon : dummyCouponData2,
      subtotal : 10,
      tax : 0.50,
      deliveryTip : 0.00,
      total : 10.50,
      orderedTimestamp : null,
      preparingTimestamp : null,
      readyTimestamp : null,
      completedTimestamp : null
    }

    let dummyOrderItemData2:OrderItem[] = [{
      orderItemId:1,
      order:dummyOrderData2,
      menuItem:dummyMenuItemData2,
      quantity:1,
      specialInstructions:"does not matter"
    }]

    beforeEach(async () => {
      await TestBed.configureTestingModule({
        imports: [
          RouterTestingModule.withRoutes([]),
          [HttpClientTestingModule]
        ],
        declarations: [ CustomerCheckoutComponent, CustomerNavBarComponent ]
      })
      .compileComponents();
    });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(
          [
            {path: 'login', component: LoginComponent}, 
            {path: 'register', component: RegisterComponent},
            {path: 'forget', component: ForgetComponent}, 
            {path: 'home', component: CustomerHomePageComponent}, 
            {path: 'checkout', component: CustomerCheckoutComponent}, 
            {path: 'cart', component: CartComponent}, 
            {path: 'menu', component: CustomerMenuComponent}, 
            {path: 'payment', component: CustomerPaymentComponent}, 
            {path: 'verified', component: CustomerOrderVerificationComponent}
            ]
        ),
        [HttpClientTestingModule]
      ],
      declarations: [ 
        CustomerCheckoutComponent, 
        CustomerNavBarComponent, 
        RegisterComponent,
        ForgetComponent,
        CustomerHomePageComponent,
        CartComponent,
        CustomerMenuComponent,
        CustomerPaymentComponent, 
        CustomerOrderVerificationComponent,
        LoginComponent
       ],
      providers: [
        {provide: OrderService, useClass:MockService},
        {provide: OrderitemService, useClass:MockService},
        {provide: DistributionService, useClass:MockService},
        {provide: CouponService, useClass:MockService},
        {provide: HttpClient, useValue: mockClient}
      ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(CustomerCheckoutComponent);
    component = fixture.componentInstance;

    spyOn(localStorage, 'getItem').and.callFake(function (key) {
      return store[key];
    });
    spyOn(localStorage, 'setItem').and.callFake(function (key, value) {
      return store[key] = value + '';
    });
    spyOn(localStorage, 'clear').and.callFake(function () {
        store = {};
    });
    
    localStorage.setItem("customerOrder", JSON.stringify(dummyOrderData2));
    localStorage.setItem("customerInfo", JSON.stringify(dummyCustomerData2));
    component.customer = JSON.parse(window.localStorage.getItem("customerInfo"));
    component.order = JSON.parse(window.localStorage.getItem("customerOrder"));

    fixture.detectChanges();

    router = TestBed.inject(Router);
    ordServ = TestBed.inject(OrderService);
    ordItemServ = TestBed.inject(OrderitemService);
    distServ = TestBed.inject(DistributionService);
    coupServ = TestBed.inject(CouponService);
    mockClient = TestBed.get(HttpClient);

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have h1 say Customer Checkout', ()=>{
    // fixture.detectChanges();
    let checkoutHeader = fixture.debugElement.query(By.css('#pageheader')).nativeElement;
    expect(checkoutHeader.innerHTML).toBe('Customer Checkout');
  });

  it('should start with subtotal, tax, and total populated', ()=>{
    let subtot = fixture.debugElement.query(By.css('#subtotal')).nativeElement;
    let tax = fixture.debugElement.query(By.css('#tax')).nativeElement;
    let tot = fixture.debugElement.query(By.css('#total')).nativeElement;
    expect(subtot.innerHTML).toBe('$10.00');
    expect(tax.innerHTML).toBe('$0.50');
    expect(tot.innerHTML).toBe('$10.50');
  });

  it('should have tip show up when entered', ()=>{
    component.deliveryTip = 2;
    fixture.detectChanges();
    let tip = fixture.debugElement.query(By.css('#tip')).nativeElement;
    expect(tip.innerHTML).toBe('$2.00');
  });

  it('should go to payment when proceed to payment is clicked', waitForAsync(()=> {
    spyOn(distServ, 'getDistributionById').and.returnValue(Observable.create(observer=>{
      observer.next(dummyDistributionData2);
    }));

    spyOn(ordServ, 'updateOrder').and.returnValue(Observable.create(observer=>{
      observer.next("order updated");
    }));
    const navigateSpy = spyOn(router, 'navigate');
    // let submitButton = fixture.debugElement.query(By.css('#submitorder')).nativeElement;
    // submitButton.click();
    component.submitorder();
    fixture.detectChanges();
    expect(navigateSpy).toHaveBeenCalledWith(['/payment']);
  }));

  it('should hide the tip field when pickup is selected', waitForAsync(()=> {
    // let pickupButton = fixture.debugElement.query(By.css('#pickupOption')).nativeElement;
    // pickupButton.click();
    component.newDistr(2);
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      expect(component.isVisible).toBe(false);
    });
  }));

  it('should navgiate to cart when gotocart button is clicked', waitForAsync(()=> {
    // let pickupButton = fixture.debugElement.nativeElement.querySelector('#backtocart');
    // pickupButton.click();
    const navigateSpy = spyOn(router, 'navigate');
    component.backtocart();
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      expect(navigateSpy).toHaveBeenCalledWith(['/cart']);
    });
  }));

  it('should show a discount when a coupon is entered', waitForAsync(()=> {
    spyOn(coupServ, 'getCouponById').and.returnValue(Observable.create(observer=>{
      observer.next(dummyCouponData2);
    }));
    spyOn(ordItemServ, 'getOrderItemsByOrderId').and.returnValue(Observable.create(observer=>{
      observer.next(dummyOrderItemData2);
    }));
    component.newCoupon("baconspecial");
    fixture.detectChanges();
    let discount = fixture.debugElement.query(By.css('#discount')).nativeElement;
    fixture.whenStable().then(()=> {
      expect(discount.innerHTML).toBe('-$1.00');
    });
  }));

});

import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationStart, Router, Event } from '@angular/router';
import { Customer } from '../services/customer';
import { CustomerService } from '../services/customer.service';
import { Order } from '../services/order';
import { OrderService } from '../services/order.service';

@Component({
  selector: 'app-customer-order-verification',
  templateUrl: './customer-order-verification.component.html',
  styleUrls: ['./customer-order-verification.component.css']
})
export class CustomerOrderVerificationComponent implements OnInit, OnDestroy {

  constructor(private router:Router, 
    private custServ:CustomerService,
    private ordServ:OrderService) {}

  customer:Customer;
  order:Order;
  custname:String;
  distributionMethod:String;
  message1:String;
  message2:String;
  customerId:number;

  //we get the order/customer data from session data
  ngOnInit(): void {
    this.customer = JSON.parse(localStorage.getItem("customerInfo"));
    this.order = JSON.parse(localStorage.getItem("customerOrder"));
    if (this.customer == null) {
      this.router.navigate(['/login']);
    }
    this.customerId = this.customer.customerId;
    this.displayContent();
  }

  //if they leave this page we create a new order in build status so they can place another order
  ngOnDestroy():void{
    this.ordServ.enterNewOrder(this.customerId).subscribe(
      response => {
        this.ordServ.getNewOrder(this.customerId).subscribe(
          response => {
            this.order = response;
            localStorage.setItem("customerOrder", JSON.stringify(this.order));
            this.order = JSON.parse(localStorage.getItem("customerOrder"));
          }
        )
      }
    )
  }

  //this displays the message data depending on the distribution method
  displayContent() {
    this.custname = this.customer.firstName + " " + this.customer.lastName;
    this.distributionMethod = this.order.distribution.distributionMethod;
    if (this.order.distribution.distributionId == 1) {
      this.message1 = "Your order will be delivered in roughly 50 minutes";
      this.message2 = "to " + this.customer.address + " " + this.customer.city + ", " + this.customer.state + " " + this.customer.zipCode;
    } else {
      this.message1 = "Your order will be ready for pickup in roughly 20 minutes";
    }

  }

  //routes back to the home page
  backtohome() {
    this.router.navigate(['/home']);
  }


}

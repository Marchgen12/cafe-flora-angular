import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs';
import { CustomerNavBarComponent } from '../customer-nav-bar/customer-nav-bar.component';
import { Coupon } from '../services/coupon';
import { CouponService } from '../services/coupon.service';
import { CouponStatus } from '../services/couponstatus';
import { Customer } from '../services/customer';
import { CustomerService } from '../services/customer.service';
import { Distribution } from '../services/distribution';
import { DistributionService } from '../services/distribution.service';
import { MenuItem } from '../services/menuitem';
import { MockBank } from '../services/mockbank';
import { Order } from '../services/order';
import { OrderService } from '../services/order.service';
import { OrderitemService } from '../services/orderitem.service';
import { OrderStatus } from '../services/orderstatus';

import { CustomerOrderVerificationComponent } from './customer-order-verification.component';

describe('CustomerOrderVerificationComponent', () => {
  class MockService {
    enterNewOrder() {return mockClient.post()}
    getNewOrder() {return mockClient.get()}
  }

  let component: CustomerOrderVerificationComponent;
  let fixture: ComponentFixture<CustomerOrderVerificationComponent>;
  let mockClient: {get: jasmine.Spy, post: jasmine.Spy, put: jasmine.Spy, delete: jasmine.Spy}
  let router:Router;
  let ordServ:OrderService;
  let ordItemServ:OrderitemService;
  let distServ:DistributionService;
  let coupServ:CouponService;
  let store = {};
  let httpMock: HttpTestingController;
  let custServ:CustomerService;

  let dummyCustomerData2:Customer = {
    customerId : 1,
    email : "email@email.com",
    password : "password",
    firstName : "John",
    lastName : "Smith",
    address : "123 Main St",
    city : "New City",
    state : "NY",
    zipCode : "01102",
    phone : "616 314 2589",
    creditCardNumber : "1111111111111111",
    creditCardName : "John Smith",
    creditCardMonth : 1,
    creditCardYear : 2021,
    creditCardCode : 999
    }
  
    let dummyOrderStatusData2:OrderStatus = {
      statusId:1,
      statusName:"Building"
    }
    
    let dummyDistributionData2:Distribution = {
      distributionId : 1,
      distributionMethod : "Delivery"
    }
    
    let dummyCouponStatusData2:CouponStatus = {
      couponStatusId : 1,
      couponStatus : "Active"
    }
    
    let dummyMenuItemData2:MenuItem = {
      itemId : 1,
      itemName : "Bacon",
      itemDescription : "Nice Bacon",
      itemCost : 4.50,
      itemImageUrl : "thisurl"
    }
    
    let dummyCouponData2:Coupon = {
      couponCode : "baconspecial",
      menuItem : dummyMenuItemData2,
      couponStatus : dummyCouponStatusData2,
      couponAmount : -1.00
    }
    
    let dummyOrderData2:Order = {
      orderId : 1,
      customer : dummyCustomerData2,
      orderStatus : dummyOrderStatusData2,
      distribution : dummyDistributionData2,
      coupon : dummyCouponData2,
      subtotal : 10,
      tax : 0.50,
      deliveryTip : 0.00,
      total : 10.50,
      orderedTimestamp : null,
      preparingTimestamp : null,
      readyTimestamp : null,
      completedTimestamp : null
    }

    let dummyMockBankData2:MockBank = {
      creditCardNumber:"1111111111111111",
      creditAmount: 2000,
      creditCardName:"Daniel Butcher",
      creditCardCode: 999,
      creditCardMonth: 1,
      creditCardYear: 2025
    }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerOrderVerificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
        [HttpClientTestingModule]
      ],
      declarations: [ CustomerOrderVerificationComponent, CustomerNavBarComponent ],
      providers: [
        {provide: OrderService, useClass:MockService},
        {provide: OrderitemService, useClass:MockService},
        {provide: CustomerService, useClass:MockService},
        {provide: HttpClient, useValue: mockClient}
      ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(CustomerOrderVerificationComponent);
    component = fixture.componentInstance;

    spyOn(localStorage, 'getItem').and.callFake(function (key) {
      return store[key];
    });
    spyOn(localStorage, 'setItem').and.callFake(function (key, value) {
      return store[key] = value + '';
    });
    spyOn(localStorage, 'clear').and.callFake(function () {
        store = {};
    });
    
    localStorage.setItem("customerOrder", JSON.stringify(dummyOrderData2));
    localStorage.setItem("customerInfo", JSON.stringify(dummyCustomerData2));
    component.customer = JSON.parse(window.localStorage.getItem("customerInfo"));
    component.order = JSON.parse(window.localStorage.getItem("customerOrder"));

    fixture.detectChanges();
    
    router = TestBed.inject(Router);
    ordServ = TestBed.inject(OrderService);

    mockClient = TestBed.get(HttpClient);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should start with fields populated', ()=>{
    let customer = fixture.debugElement.query(By.css('#customer')).nativeElement;
    let distribution = fixture.debugElement.query(By.css('#distribution')).nativeElement;
    let message1 = fixture.debugElement.query(By.css('#message1')).nativeElement;
    let message2 = fixture.debugElement.query(By.css('#message2')).nativeElement;
    expect(customer.innerHTML).toBe('John Smith');
    expect(distribution.innerHTML).toBe('Delivery');
    expect(message1.innerHTML).toBe('Your order will be delivered in roughly 50 minutes');
    expect(message2.innerHTML).toBe('to 123 Main St New City, NY 01102');
  });

  it('should go to home page when close is clicked', waitForAsync(()=> {
    spyOn(ordServ, 'enterNewOrder').and.returnValue(Observable.create(observer=>{
      observer.next("anything");
    }));
    spyOn(ordServ, 'getNewOrder').and.returnValue(Observable.create(observer=>{
      observer.next(dummyOrderData2);
    }));
    const navigateSpy = spyOn(router, 'navigate');
    component.backtohome();
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      expect(navigateSpy).toHaveBeenCalledWith(['/home']);
    });
  }));

});

import { CouponStatus } from "./couponstatus";
import { MenuItem } from "./menuitem";

export interface Coupon {
    'couponCode':String;
    'menuItem':MenuItem;
    'couponStatus':CouponStatus;
    'couponAmount':number;
}
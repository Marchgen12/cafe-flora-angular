import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Coupon } from './coupon';

@Injectable({
  providedIn: 'root'
})
export class CouponService {

  // private urlbase = "http://ec2-3-17-207-133.us-east-2.compute.amazonaws.com:9025/";
  private urlbase = "//localhost:9025/";

  private url1=this.urlbase + "coupons/individualcoupons/";

  constructor(private httpClie: HttpClient) { }

  getCouponById(couponCode):Observable<Coupon> {
    return this.httpClie.get<Coupon>(this.url1 + couponCode);
  }
}

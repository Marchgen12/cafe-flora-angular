// export interface Customer {
//     'customerId':number;
//     'email':String;
//     'password':String;
//     'firstName':String;
//     'lastName':String;
//     'address':String;
//     'city':String;
//     'state':String;
//     'zipCode':String;
//     'phone':String;
//     'creditCardNumber':String;
//     'creditCardName':String;
//     'creditCardCode':number;
// }

export class Customer{
     'customerId':number;
     'email':String;
     'password':String;
     'firstName':String;
     'lastName':String;
     'address':String;
     'city':String;
     'state':String;
     'zipCode':String;
     'phone':String;
     'creditCardNumber':String;
     'creditCardName':String;
     'creditCardMonth':number;
     'creditCardYear':number;
     'creditCardCode':number;

    // constructor(customerId:number, email:String, password:String, firstName:String, lastName:String, address:String, city:String,
    //     state:String, zipCode:String, phone:String, creditCardNumber:String, creditCardName:String, creditCardMonth:number, creditCardYear:number creditCardCode:number){}
}

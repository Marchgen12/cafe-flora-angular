import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OrderItem } from './orderitem';
import { MenuItem } from './menuitem';
import { Order } from './order';

@Injectable({
  providedIn: 'root'
})
export class OrderitemService {

  // private urlbase = "http://ec2-3-17-207-133.us-east-2.compute.amazonaws.com:9025/";
  private urlbase = "//localhost:9025/";

  // get by order Id
  private url1=this.urlbase + "orderitems/orderorderitems/";

  // post/put request
  private url2=this.urlbase + "orderitems/";

  // get individual orderItem by order and menu #
  private url3=this.urlbase + "orderitems/individualordermenuitems/";

  // get individual orderItem by orderItem id
  private url4=this.urlbase + "orderitems/individualorderitems/";

  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'responseType': 'text' as 'json'
  });


  constructor(private httpClie: HttpClient) { }

  getOrderItemsByOrderId(orderId):Observable<OrderItem[]> {
    return this.httpClie.get<OrderItem[]>(this.url1 + orderId);
  }

  getOrderItemByOrderAndMenuId(orderId, menuItemId): Observable<OrderItem> {
    //console.log(this.url3 + orderId + "/" + menuItemId);
    return this.httpClie.get<OrderItem>(this.url3 + orderId + "/" + menuItemId);
  }

  getOrderItemById(orderItemId): Observable<OrderItem> {
    return this.httpClie.get<OrderItem>(this.url4 + orderItemId);
  }

  createOrderItem(menuId, orderId): Observable<String> {
    menuId = menuId.toString();
    orderId = orderId.toString();
    let quantity = "1";
    let map = {"orderId": orderId, "menuItemId": menuId, "quantity": quantity, "specialInstructions": null};
    return this.httpClie.post<String>(this.url2, JSON.stringify(map), {responseType: "text" as "json", headers: this.headers});
  }

  addOrderItem(orderItemId, quantity): Observable<String> {
    orderItemId = orderItemId.toString();
    quantity = quantity+1;
    let map = {"orderItemId": orderItemId, "quantity": quantity, "specialInstructions": null};
    return this.httpClie.put<String>(this.url2, JSON.stringify(map), {responseType: "text" as "json", headers: this.headers});
  }
  
  updateOrderItem(orderItemId, quantity, specialInstructions): Observable<String> {
    orderItemId = orderItemId.toString();
    quantity = quantity+1;
    let map = {"orderItemId": orderItemId, "quantity": quantity, "specialInstructions": specialInstructions};
    return this.httpClie.put<String>(this.url2, JSON.stringify(map), {headers: this.headers,
      responseType:"text" as "json"
    });
  }

  removeOrderItem(orderItemId): Observable<void> {
    return this.httpClie.delete<void>(this.url2 + "/orderitems/" + orderItemId);
  }

  removeAllOrderItems(orderId): Observable<void>{
    return this.httpClie.delete<void>(this.url2 + "/orders/" + orderId, {
      responseType:"text" as "json"
    });
  };

}

import { Injectable } from '@angular/core';
import { Customer } from './customer';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EmailValidator } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  // private urlbase = "http://ec2-3-17-207-133.us-east-2.compute.amazonaws.com:9025/";
  private urlbase = "//localhost:9025/";

  private urlGetAccount=this.urlbase + "customer/account/";
  private urlPostAccount=this.urlbase + "customer/account/";
  private urlCreateAccount=this.urlbase + "customer/register/";
  private urlLogout=this.urlbase + "customer/logouts/";
  private urlVerify=this.urlbase + "customer/verifycreditcards/";
  private urlRecovery=this.urlbase + "customer/retrievepassword/";

  constructor(private httpClie: HttpClient) { }

  getCustomerById(customerId):Observable<Customer> {
    return this.httpClie.get<Customer>(this.urlGetAccount + customerId);
  }

  LoginCustomer(email:String, password:String):Observable<Customer>{
    return this.httpClie.post<Customer>(this.urlPostAccount, {
      'email':email,
      'password':password
    });
  }

  RegisterCustomer(email:String, password:String, firstName:String, lastName:String, address:String,
      city:String, state:String, zipCode:String, phone:String, creditCardName:String, creditCardNumber:String,
      creditCardMonth:number, creditCardYear:number, creditCardCode:number):Observable<Customer>{
    const httpoptions = {responseType: "text" as "json"}
    return this.httpClie.post<Customer>(this.urlCreateAccount, {
      'email':email,
      'password':password,
      'firstName':firstName,
      'lastName':lastName,
      'address':address,
      'city':city,
      'state':state,
      'zipCode':zipCode,
      'phone':phone,
      'creditCardNumber':creditCardNumber,
      'creditCardName':creditCardName,
      'creditCardMonth':creditCardMonth,
      'creditCardYear':creditCardYear,
      'creditCardCode':creditCardCode
    }, httpoptions);
  }

  RecoveryPassword(email:String, firstName:String, lastName:String):Observable<String>{
    const httpoptions = {responseType: "text" as "json"}
    return this.httpClie.post<String>(this.urlRecovery, {
      'email':email,
      'firstName':firstName,
      'lastName':lastName
    }, httpoptions);
  }

  logOut():Observable<String> {
    const httpoptions = {responseType: "text" as "json"}
    return this.httpClie.post<String>(this.urlLogout, httpoptions);
  }

  verifyCC(creditCardNumber, creditCardName, creditCardCode, creditCardMonth, creditCardYear, total):Observable<String> {
    const httpoptions = {responseType: "text" as "json"}
    return this.httpClie.post<String>(this.urlVerify, {
      'creditCardNumber':creditCardNumber,
      'creditCardName':creditCardName,
      'creditCardCode':creditCardCode,
      'creditCardMonth':creditCardMonth,
      'creditCardYear':creditCardYear,
      'total':total
    }, httpoptions);
  }
}

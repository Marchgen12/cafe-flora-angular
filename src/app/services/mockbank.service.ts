import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Mock } from 'protractor/built/driverProviders';
import { Observable } from 'rxjs';
import { MockBank } from './mockbank';

@Injectable({
  providedIn: 'root'
})
export class MockbankService {

  // private urlbase = "http://ec2-3-17-207-133.us-east-2.compute.amazonaws.com:9025/";
  private urlbase = "//localhost:9025/";

  private url1 = this.urlbase + "mockbanks/creditcardnumbers/";
  private url2 = this.urlbase + "mockbanks/creditcardname/";
  private url3 = this.urlbase + "mockbanks/creditcardamount/";

  constructor(private httpClie: HttpClient) { }

  getCreditInfoByNumber(creditCardNumber):Observable<MockBank> {
    return this.httpClie.get<MockBank>(this.url1 + creditCardNumber);
  }

  // create the method for getting card's name
  getCreditInfoByName(creditCardName): Observable<MockBank>{
    return this.httpClie.get<MockBank>(this.url2 + creditCardName);
  }

  // create the method for getting card's amount
  getCreditInfoByAmount(creditCardAmount): Observable<MockBank>{
    return this.httpClie.get<MockBank>(this.url3 + creditCardAmount);
  }
}

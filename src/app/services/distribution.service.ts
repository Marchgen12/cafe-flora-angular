import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Distribution } from './distribution';

@Injectable({
  providedIn: 'root'
})
export class DistributionService {
  // private urlbase = "http://ec2-3-17-207-133.us-east-2.compute.amazonaws.com:9025/";
  private urlbase = "//localhost:9025/";
  
  private url1=this.urlbase + "distributions/individualdistributions/";

  constructor(private httpClie: HttpClient) { }

  getDistributionById(distributionId):Observable<Distribution> {
    return this.httpClie.get<Distribution>(this.url1 + distributionId);
  }
}

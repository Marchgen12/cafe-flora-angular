import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Customer } from '../services/customer';
import { Order } from '../services/order';

@Component({
  selector: 'app-customer-home-page',
  templateUrl: './customer-home-page.component.html',
  styleUrls: ['./customer-home-page.component.css']
})
export class CustomerHomePageComponent implements OnInit, OnDestroy {
  // the documentation of the following images -> https://picsum.photos/
  /*
    437 -> place for a cafe
    163 -> table place for relax outside
  */
  images = [163, 152, 459, 89].map((n) => `https://picsum.photos/id/${n}/1050/850`); // should replace for s3 buckets of flower, sunny, grass pictures
  constructor(private router:Router) { }
  
  customer:Customer;
  order:Order;

  ngOnInit(): void {
    this.customer = JSON.parse(localStorage.getItem("customerInfo")); // this saves the current user's info
    this.order = JSON.parse(localStorage.getItem("customerOrder"));
    if (this.customer == null) {
      this.router.navigate(['/login']);
    }
  }

  ngOnDestroy():void{
    // localStorage.clear(); // this is necessary to clear up the pass storage
  }
}

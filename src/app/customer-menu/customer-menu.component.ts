import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuitemService } from '../services/menuitem.service';
import { OrderitemService } from '../services/orderitem.service';
import { OrderService } from '../services/order.service';
import { MenuItem } from '../services/menuitem';
import { Order } from '../services/order';

@Component({
  selector: 'app-customer-menu',
  templateUrl: './customer-menu.component.html',
  styleUrls: ['./customer-menu.component.css']
})
export class CustomerMenuComponent implements OnInit {

  // all items
  items: any;
  // specific item determined by click
  item: any;
  response1: any;
  response2: any;
  message:string;

  constructor(private router:Router, 
    private menuItemServ: MenuitemService, 
    private orderItemServ: OrderitemService, 
    private orderServ: OrderService) { }

    currentOrder: any;
    orderId: number;
  
  //get orderId from session variable.
  ngOnInit(): void {
    this.menuitemsGetAll();
    this.currentOrder = JSON.parse(localStorage.getItem("customerOrder"));
    this.checkCurrentOrder(this.currentOrder);
    this.orderId = this.currentOrder.orderId;
  }
  
  // OrderItems are attached to the Order local variable. They can all be
  // retrieved by calling the getOrderItemsByOrder method for the current order
  addToCart(food: MenuItem) {
    this.orderItemServ.getOrderItemByOrderAndMenuId(this.orderId, food.itemId).subscribe(
      response => {
          this.orderItemServ.addOrderItem(response.orderItemId, response.quantity).subscribe(
            res1 => {
                //this.response1 = res1;
            }
          );
      },
      (err)=>this.orderItemServ.createOrderItem(food.itemId, this.orderId).subscribe(
        res2 => {
          //this.response2 = res2;
        }
      )//,
      //()=>console.log("Done")
    );
    console.clear();
    this.message = `${food.itemName} has been added to cart`;
    setTimeout(()=>{ this.message = "" }, 1500)
  }

  menuitemsGetAll() {
    this.menuItemServ.getAllMenuItems().subscribe(
      response => {
        this.items = response;
      }
    )
  }

  menuItemsGetById(itemId: number) {
    this.menuItemServ.getMenuItemById(itemId).subscribe(
      response => {
        this.item = response;
      }
    )
  }

  gotocart() {
    this.router.navigate(['/cart']);
  }

  checkCurrentOrder(order: Order) {
    if(order == null) {
      this.router.navigate(['/login']);
    }
  }

}

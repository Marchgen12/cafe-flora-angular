import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs';
import { CustomerNavBarComponent } from '../customer-nav-bar/customer-nav-bar.component';
import { CustomerPaymentComponent } from '../customer-payment/customer-payment.component';
import { LoginComponent } from '../login/login.component';
import { CustomerService } from '../services/customer.service';
import { OrderService } from '../services/order.service';
import { OrderitemService } from '../services/orderitem.service';

import { ForgetComponent } from './forget.component';

describe('ForgetComponent', () => {
  class MockService {
    RecoveryPassword() {return mockClient.post()}
  }
  
  let component: ForgetComponent;
  let fixture: ComponentFixture<ForgetComponent>;
  let mockClient: {get: jasmine.Spy, post: jasmine.Spy, put: jasmine.Spy, delete: jasmine.Spy}
  let router:Router;
  let store = {};
  let httpMock: HttpTestingController;
  let custServ:CustomerService;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{path: 'login', component: LoginComponent}]),
        [HttpClientTestingModule],
        [FormsModule]
      ],
      declarations: [ ForgetComponent, CustomerNavBarComponent, LoginComponent],
      providers: [
        {provide: CustomerService, useClass:MockService},
        {provide: HttpClient, useValue: mockClient}
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ForgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    custServ = TestBed.inject(CustomerService);
    mockClient = TestBed.get(HttpClient);
    router = TestBed.inject(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should send email and return to login', () => {
    spyOn(custServ, 'RecoveryPassword').and.returnValue(Observable.create(observer=>{
      observer.next("string");
    }));
    const navigateSpy = spyOn(router, 'navigate');
    component.userEmail = "email";
    component.userFirstName = "firstname";
    component.userLastName = "lastname";
    let submitButton = fixture.debugElement.query(By.css('#submit')).nativeElement;
    submitButton.click();
    expect(navigateSpy).toHaveBeenCalledWith(['/login']);
  });

  it('should navigate to login', () => {
    const navigateSpy = spyOn(router, 'navigate');
    component.goLogin();
    expect(navigateSpy).toHaveBeenCalledWith(['/login']);
  });

});

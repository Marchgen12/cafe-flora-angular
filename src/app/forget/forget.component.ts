import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../services/customer.service';

@Component({
  selector: 'app-forget',
  templateUrl: './forget.component.html',
  styleUrls: ['./forget.component.css']
})
export class ForgetComponent implements OnInit {

  userEmail:string;
  userFirstName:string;
  userLastName:string;
  badMsg="";

  constructor(private CustomerService: CustomerService, private router:Router) { }

  ngOnInit(): void {
  }

  goLogin() {
    this.router.navigate(['/login']);
  }

  forgetPassword(){
    this.CustomerService.RecoveryPassword(this.userEmail, this.userFirstName, this.userLastName).subscribe(
      Response=>{
        // alert("Email was sent to the user!");
        this.router.navigate(["/login"]);
      },
      error=>{
        // alert("Email was not a valid email try again");
        this.badMsg = "Email was not a valid email try again";
      }
    )
  }
}

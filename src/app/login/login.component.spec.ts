import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { observable, Observable } from 'rxjs';
import { servicesVersion } from 'typescript';
import { CustomerHomePageComponent } from '../customer-home-page/customer-home-page.component';
import { CustomerService } from '../services/customer.service';
import { OrderService } from '../services/order.service';
import { OrderitemService } from '../services/orderitem.service';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {

  class MockService{
    LoginCustomer() {return mockClient.post()}
    enterNewOrder() {return mockClient.post()}
    getNewOrder() {return mockClient.get()}
    removeAllOrderItems() {return mockClient.delete()}
  }
  
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let mockClient: {get: jasmine.Spy, post: jasmine.Spy, put: jasmine.Spy, delete: jasmine.Spy}
  let router:Router;
  let crServ:CustomerService;
  let orServ:OrderService;
  let orItemServ:OrderitemService;
  let httpMock: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[
        RouterTestingModule.withRoutes([
          {path: 'login', component: LoginComponent},
          {path: 'home', component: CustomerHomePageComponent}
        ]),
        [HttpClientTestingModule],
        [FormsModule]
      ],
      declarations: [LoginComponent],
      providers:[
        {provide: CustomerService, useClass:MockService},
        {provide: OrderService, useClass:MockService},
        {provide: OrderitemService, useClass:MockService},
        {provide: HttpClient, useValue: mockClient}
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    crServ = TestBed.inject(CustomerService);
    orServ = TestBed.inject(OrderService);
    orItemServ = TestBed.inject(OrderitemService);
    mockClient = TestBed.get(HttpClient)
    router = TestBed.inject(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should login user', () =>{
    spyOn(crServ, 'LoginCustomer').and.returnValue(Observable.create(observer=>{
      observer.next("string");
    }));

    spyOn(orServ, 'enterNewOrder').and.returnValue(Observable.create(observer=>{
      observer.next("string");
    }));

    spyOn(orServ, 'getNewOrder').and.returnValue(Observable.create(observer=>{
      observer.next("string");
    }));

    spyOn(orItemServ, 'removeAllOrderItems').and.returnValue(Observable.create(observer=>{
      observer.next("string");
    }));

    const navigateSpy = spyOn(router, 'navigate');
    component.customer.email = "email@email.com";
    component.customer.password = "password";

    fixture.detectChanges();

    component.loggingIn();

    expect(navigateSpy).toHaveBeenCalledWith(['/home']);
  });

  it('should go back to login page', ()=>{
    const navigateSpy = spyOn(router, 'navigate');
    component.goRegister();
    expect(navigateSpy).toHaveBeenCalledWith(['/register']);
  });

  it('should go back to login page', ()=>{
    const navigateSpy = spyOn(router, 'navigate');
    component.goForgetPassword();
    expect(navigateSpy).toHaveBeenCalledWith(['/forget']);
  });

});

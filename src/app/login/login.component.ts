import { Component, OnInit } from '@angular/core';
import { NgForm} from '@angular/forms';
import { Router } from '@angular/router';
import { Customer} from 'src/app/services/customer';
import { CustomerService} from 'src/app/services/customer.service';
import { Order } from '../services/order';
import { OrderService } from '../services/order.service';
import { OrderitemService } from '../services/orderitem.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  pageTitle = "Login page";

  badMsg = '';

  customer = new Customer();
  customerId:number;
  order:Order;
  orderId:number;
  // user:Customer[]; //I whiched the interface to class so i can access the variables

  constructor(private CustomerService: CustomerService, private ordServ:OrderService, private ordItemServ:OrderitemService, private router:Router) { }

  ngOnInit(): void {
  }

  goRegister(){
    this.router.navigate(['/register']);
  }

  goForgetPassword(){
    this.router.navigate(['/forget']);
  }

  loggingIn(){
    this.CustomerService.LoginCustomer(this.customer.email, this.customer.password).subscribe(
      response=>{
        localStorage.setItem("customerInfo", JSON.stringify(response));
        this.customer = response;
        this.customerId = this.customer.customerId;
        this.ordServ.enterNewOrder(this.customerId).subscribe(
          response => {
            this.ordServ.getNewOrder(this.customerId).subscribe(
              response => {
                this.order = response;
                this.orderId = this.order.orderId;
                localStorage.setItem("customerOrder", JSON.stringify(response));
                this.ordItemServ.removeAllOrderItems(this.orderId).subscribe(
                  response => {
                    this.router.navigate(['/home']);
                  }
                )
              }
            )
          }
        )
      },
      error =>{
        this.badMsg = "Wrong Credetial, Bad Login or password. Try again";
        this.router.navigate(['/login']);
      }
    );
  }
}

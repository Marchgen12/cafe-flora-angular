import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs';
import { Coupon } from '../services/coupon';
import { CouponStatus } from '../services/couponstatus';
import { Customer } from '../services/customer';
import { Distribution } from '../services/distribution';
import { MenuItem } from '../services/menuitem';
import { Order } from '../services/order';
import { OrderService } from '../services/order.service';
import { OrderStatus } from '../services/orderstatus';
import { CustomerNavBarComponent } from './customer-nav-bar.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { BrowserModule, By } from '@angular/platform-browser';
import { CartComponent } from '../cart/cart.component';
import { CustomerCheckoutComponent } from '../customer-checkout/customer-checkout.component';
import { CustomerHomePageComponent } from '../customer-home-page/customer-home-page.component';
import { CustomerMenuComponent } from '../customer-menu/customer-menu.component';
import { CustomerOrderVerificationComponent } from '../customer-order-verification/customer-order-verification.component';
import { CustomerPaymentComponent } from '../customer-payment/customer-payment.component';
import { ForgetComponent } from '../forget/forget.component';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('CustomerNavBarComponent', () => {
  class MockService {
    deleteOrder() {return mockClient.delete()}
  }

  let component: CustomerNavBarComponent;
  let fixture: ComponentFixture<CustomerNavBarComponent>;
  let router:Router;
  let mockClient: { get: jasmine.Spy, post: jasmine.Spy, put: jasmine.Spy, delete: jasmine.Spy };
  let store = {};
  let httpMock: HttpTestingController;

  let dummyCustomerData = {
    customerId: 1,
    email: "email@email.com",
    password: "password",
    firstName: "John",
    lastName: "Smith",
    address: "123 Main St",
    city: "New City",
    state:"NY",
    zipCode:"01102",
    phone:"616 314 2589",
    creditCardNumber:"1111111111111111",
    creditCardName:"John Smith",
    creditCardMonth:1,
    creditCardYear:2021,
    creditCardCode:999,
  }

  let dummyOrderStatusData = {
    statusId:1,
    statusName:"Building"
  }

  let dummyDistributionData = {
    distributionId:1,
    distributionMethod:"Delivery"
  }

  let dummyCouponStatusData = {
    couponStatusId:1,
    couponStatus:"Active"
  }

  let dummyMenuItemData = {
    itemId:1,
    itemName:"Bacon",
    itemDescription:"Nice Bacon",
    itemCost:4.50,
    itemImageUrl:"thisurl"
  }

  let dummyCouponData = {
    couponCode:"baconspecial",
    menuItem:dummyMenuItemData,
    couponStatus:dummyCouponStatusData,
    couponAmount:-1.00
  }

  let dummyOrderData = {
    orderId: 1,
    customer: dummyCustomerData,
    orderStatus: dummyOrderStatusData,
    distribution: dummyDistributionData,
    coupon: dummyCouponData,
    subtotal: 10,
    tax: 0.50,
    deliveryTip: 2.00,
    total: 12.50,
    orderedTimestamp:null,
    preparingTimestamp:null,
    readyTimestamp:null,
    completedTimestamp:null
  }
  let orderService:OrderService;

  let dummyCustomerData2:Customer = {
  customerId : 1,
  email : "email@email.com",
  password : "password",
  firstName : "John",
  lastName : "Smith",
  address : "123 Main St",
  city : "New City",
  state : "NY",
  zipCode : "01102",
  phone : "616 314 2589",
  creditCardNumber : "1111111111111111",
  creditCardName : "John Smith",
  creditCardMonth : 1,
  creditCardYear : 2021,
  creditCardCode : 999
  }

  let dummyOrderStatusData2:OrderStatus = {
    statusId:1,
    statusName:"Building"
  }



  let dummyDistributionData2:Distribution = {
    distributionId : 1,
    distributionMethod : "Delivery"
  }

  let dummyCouponStatusData2:CouponStatus = {
    couponStatusId : 1,
    couponStatus : "Active"
  }



  let dummyMenuItemData2:MenuItem = {
    itemId : 1,
    itemName : "Bacon",
    itemDescription : "Nice Bacon",
    itemCost : 4.50,
    itemImageUrl : "thisurl"
  }



  let dummyCouponData2:Coupon = {
    couponCode : "baconspecial",
    menuItem : dummyMenuItemData2,
    couponStatus : dummyCouponStatusData2,
    couponAmount : -1.00
  }



  let dummyOrderData2:Order = {
    orderId : 1,
    customer : dummyCustomerData2,
    orderStatus : dummyOrderStatusData2,
    distribution : dummyDistributionData2,
    coupon : dummyCouponData2,
    subtotal : 10,
    tax : 0.50,
    deliveryTip : 2.00,
    total : 12.50,
    orderedTimestamp : null,
    preparingTimestamp : null,
    readyTimestamp : null,
    completedTimestamp : null
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
        [HttpClientTestingModule]
      ],
      declarations: [ CustomerNavBarComponent ],
      providers: [
        {provide: OrderService, useClass:MockService},
        {provide: HttpClient, useValue: mockClient}
      ]
    })
    .compileComponents();


  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(
          [
            {path: 'login', component: LoginComponent}, 
            {path: 'register', component: RegisterComponent},
            {path: 'forget', component: ForgetComponent}, 
            {path: 'home', component: CustomerHomePageComponent}, 
            {path: 'checkout', component: CustomerCheckoutComponent}, 
            {path: 'cart', component: CartComponent}, 
            {path: 'menu', component: CustomerMenuComponent}, 
            {path: 'payment', component: CustomerPaymentComponent}, 
            {path: 'verified', component: CustomerOrderVerificationComponent}
            ],
        ),
        [HttpClientTestingModule],
        [BrowserModule],
        [ReactiveFormsModule],
        [FormsModule],
        [HttpClientModule],
        [NgbModule]
      ],
      declarations: [ 
        CustomerCheckoutComponent, 
        CustomerNavBarComponent, 
        RegisterComponent,
        ForgetComponent,
        CustomerHomePageComponent,
        CartComponent,
        CustomerMenuComponent,
        CustomerPaymentComponent, 
        CustomerOrderVerificationComponent,
        LoginComponent
       ],
      providers: [
        {provide: OrderService, useClass:MockService},
        {provide: HttpClient, useValue: mockClient}
      ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(CustomerNavBarComponent);
    component = fixture.componentInstance;

    spyOn(localStorage, 'getItem').and.callFake(function (key) {
      return store[key];
    });
    spyOn(localStorage, 'setItem').and.callFake(function (key, value) {
      return store[key] = value + '';
    });
    spyOn(localStorage, 'clear').and.callFake(function () {
        store = {};
    });



    // spyOn(window.localStorage, 'setItem');
    // window.localStorage.setItem('customerInfo', JSON.stringify(dummyCustomerData2));
    // window.localStorage.setItem('customerOrder', JSON.stringify(dummyOrderData2));


    localStorage.setItem("customerOrder", JSON.stringify(dummyOrderData2));
    localStorage.setItem("customerInfo", JSON.stringify(dummyCustomerData2));
    component.customer = JSON.parse(window.localStorage.getItem("customerInfo"));
    component.order = JSON.parse(window.localStorage.getItem("customerOrder"));

    fixture.detectChanges();
    router = TestBed.inject(Router);
    orderService = TestBed.inject(OrderService);
    mockClient = TestBed.get(HttpClient);

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to home',  waitForAsync(() => {
    const navigateSpy = spyOn(router, 'navigate');
    // let goHomeButton = fixture.debugElement.query(By.css('#home')).nativeElement;
    // goHomeButton.click();
    fixture.detectChanges();

    component.goHome();
    fixture.whenStable().then(()=> {
      expect(navigateSpy).toHaveBeenCalledWith(['/home']);
    })
  }));

  it('should navigate to menu', () => {
    const navigateSpy = spyOn(router, 'navigate');
    component.goMenu();
    expect(navigateSpy).toHaveBeenCalledWith(['/menu']);
  });

  it('should navigate to cart', () => {
    const navigateSpy = spyOn(router, 'navigate');
    component.goCart();
    expect(navigateSpy).toHaveBeenCalledWith(['/cart']);
  });

  it('should navigate to login', () => {
    const navigateSpy = spyOn(router, 'navigate');
    const dummyDelete = "Order Deleted";
    spyOn(orderService, 'deleteOrder').and.returnValue(Observable.create(observer=>{
      observer.next(dummyDelete);
    }));
    component.goLogout();
    expect(navigateSpy).toHaveBeenCalledWith(['/login']);
  });

});
